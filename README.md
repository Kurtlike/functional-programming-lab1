# Лабораторная работа #1

**Дисциплина:** "Функциональное программирование"

**Выполнил:** Рождественский Никита, P34102

**Название:** "Решение задач Project Euler"

**Цель работы:** 
Решить две задачи project euler, различными способами используя выбранный ранее функциональный ЯП.

**Требования**
1. монолитные реализации с использованием:
   - хвостовой рекурсии;
   - рекурсии (вариант с хвостовой рекурсией не является примером рекурсии);
2. модульной реализации, где явно разделена генерация последовательности, фильтрация и свёртка (должны использоваться функции reduce/fold, filter и аналогичные);
3. генерация последовательности при помощи отображения (map);
4. работа со спец. синтаксисом для циклов (где применимо);
5. работа с бесконечными списками для языков поддерживающих ленивые коллекции или итераторы как часть языка (к примеру Haskell, Clojure);
6. реализация на любом удобном для вас традиционном языке программировании для сравнения.

### Условия задач

**1. Задача 6:**
```
    Find the difference between the sum of the squares 
    of the first one hundred natural numbers and the square of the sum.
```
  
**2. Задача 25:**
```
    What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
```

#### Окружение и плагины
```
    (defproject lab1 "0.1.0-SNAPSHOT"
  :description "lab1 for functional programming"
  :license {:name "EPL-2.0 OR GPL-2.0-or-later WITH Classpath-exception-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.11.1"]]
  :main lab1.core
  :aot [lab1.core]
  :target-path "target/%s"
  :plugins [[lein-cljfmt "0.9.0"]]
  :profiles {:uberjar {:aot :all
                       :jvm-opts ["-Dclojure.compiler.direct-linking=true"]}})

```


#### Выполнения задания №6:

Монолитом:
```
(defn sum_square_difference_monolite
  "Calculates  the difference between the sum of the squares 
  and the square of the sum
  "
  [n]
  (let [sqr_l (fn [num]
                (* num num))]
    (loop [sum 0 sum_sq 0 next 0]
      (if
       (< next n)
        (recur (+ sum next) (+ sum_sq (sqr_l next)) (inc next))
        (- (sqr_l sum) sum_sq)))))

```
Модулями:
```
(defn sqr [num]
  (* num num))

(defn get_sum_squared [num]
  (sqr (reduce + 0 (range num))))

(defn get_squares_sum [num]
  (reduce + 0 (map
               (fn [seq_num] (sqr seq_num))
               (range num))))

(defn get_6_task [num]
  (-
   (get_sum_squared (+ 1 num))
   (get_squares_sum (+ 1 num))))

```
На java
```
public class Task6 {
    static BigInteger getResult(int n) {
        BigInteger squareOfSum = BigInteger.valueOf(0);
        BigInteger sumOfSquares = BigInteger.valueOf(0);
        for (int i = 0; i < n + 1; i++) {
            squareOfSum = squareOfSum.add(BigInteger.valueOf(i));
            sumOfSquares = sumOfSquares.add(BigInteger.valueOf(i * i));
        }
        squareOfSum = squareOfSum.multiply(squareOfSum);
        return squareOfSum.subtract(sumOfSquares);
    }
}
```
#### Выполнения задания №25:
Монолитом:
```
(defn n-digit-fibonacci-number_monolite
  "Calculates the index of the first term in the Fibonacci sequence to contain n digits"
  [n]
  (let [num-digits (fn [num]
                     (count (str num)))]

    (loop [prev 0 now 1 num 1]
      (if
       (< (num-digits now) n)
        (recur now (+' now prev) (inc num))
        num))))
```
Модулями:
```
(defn fibonacci []
  (map first
       (iterate
        (fn [[a b]] [b (+' a b)]) [0 1])))

(defn num-of-digits [n]
  (count (str n)))

(defn fibonacci-n-digit-number
  [num]
  (first
   (first
    (drop-while (fn [n] (< (second n) num))
                (map-indexed
                 (fn [i n] [i (num-of-digits n)]) (fibonacci))))))
```
На java
```
public class Task25 {
    public static int getResult(int n) {
        BigInteger lowerThres = BigInteger.TEN.pow(n - 1);
        BigInteger prev = BigInteger.ONE;
        BigInteger cur = BigInteger.ZERO;
        for (int i = 0; ; i++) {
            // At this point, prev = fibonacci(i - 1) and cur = fibonacci(i)
             if (cur.compareTo(lowerThres) >= 0)
                return i;

            // Advance the Fibonacci sequence by one step
            BigInteger temp = cur.add(prev);
            prev = cur;
            cur = temp;
        }
    }
}
```
Результаты выполнения
```
num = 10000
sum-square-difference tail-recursive "Elapsed time: 8.998196 msecs"
Result: 2500166641665000
num = 1000
sum-square-difference-recursive "Elapsed time: 0.90885 msecs"
Result: 250166416500
sum-square-difference-seq "Elapsed time: 10.505587 msecs"
Result: 2500166641665000
sum-square-difference-resuced "Elapsed time: 0.77147 msecs"
Result: 2500166641665000
num = 1000
n-digit-fibonacci-number tail-recursive: "Elapsed time: 5.319895 msecs"
Result: 4782
num = 500
n-digit-fibonacci-number-recursive: "Elapsed time: 1.275971 msecs"
Result: 2390
fibonacci-n-digit-number-iterative: "Elapsed time: 8.536166 msecs"
Solution: 4782
fibonacci-n-digit-number-lazy-cat: "Elapsed time: 4.526553 msecs"
Solution: 4782
```
результаты java 
```
task6Result: 2500166641665000
Time used to calculate in milliseconds: 3.942059
task25Result: 4782
Time used to calculate in milliseconds: 2.320869
```

**Заключение**

Работа, несмотря на простоту конкретных задач, довольно сложная. Очень сложно мыслить ленивыми последовательностями и читать функциональный код. Хорошо, что функции в ClojureDocs идут хоть с какими-то примерами, плохо, что требуется довольно много стандартных функций, чтоб писать даже простой код. 
