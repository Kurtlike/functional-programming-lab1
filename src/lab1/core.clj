(ns lab1.core
  (:require [lab1.task-6 :refer [report-6]]
            [lab1.task-25 :refer [report-25]]))

(defn -main []
  (report-6)
  (report-25))
