(ns lab1.task-25)

(defn fibonacci []
  (map first
       (iterate
        (fn [[a b]] [b (+' a b)]) [0 1])))

(defn num-of-digits [n]
  (count (str n)))

(defn fibonacci-n-digit-number
  [num]
  (let [big-num (.pow (BigInteger. "10") (- num 1))]
    (first
     (first
      (drop-while (fn [n] (< (second n) big-num))
                  (map-indexed
                   (fn [i n] [i n]) (fibonacci)))))))
;;

(def fib-seq-cat
  (lazy-cat [0 1] (map +' (rest fib-seq-cat) fib-seq-cat)))

(defn fibonacci-n-digit-number-lazy-cat
  [n]
  (let [big-num (.pow (BigInteger. "10") (- n 1))]
    (count (take-while #(> big-num %) fib-seq-cat))))
;;

(defn n-digit-fibonacci-number-monolite
  "Calculates the index of the first term in the Fibonacci sequence to contain n digits"
  [n]
  (let [big-num (.pow (BigInteger. "10") (- n 1))]
    (loop [prev 0 now 1 num 1]
      (if
       (<  now big-num)
        (recur now (+' now prev) (inc num))
        num))))
;;

(defn n-digit-fibonacci-number-monolite-recur
  "Calculates the index of the first term in the Fibonacci sequence to contain n digits"
  ([n]
   (n-digit-fibonacci-number-monolite-recur 0 1 1 (.pow (BigInteger. "10") (- n 1))))
  ([prev now num big-num]
   (if (< now big-num)
     (n-digit-fibonacci-number-monolite-recur now (+' now prev) (inc num) big-num)
     num)))
;;

(defn report-25 []
  (println "num = 1000")
  (print "n-digit-fibonacci-number tail-recursive: ")
  (time (n-digit-fibonacci-number-monolite 1000))
  (println "Result:" (n-digit-fibonacci-number-monolite 1000))
  (println "num = 500")
  (print "n-digit-fibonacci-number-recursive: ")
  (time (n-digit-fibonacci-number-monolite-recur 500))
  (println "Result:" (n-digit-fibonacci-number-monolite-recur 500))
  (print "fibonacci-n-digit-number-iterative: ")
  (time (fibonacci-n-digit-number 1000))
  (println "Solution:" (fibonacci-n-digit-number 1000))
  (print "fibonacci-n-digit-number-lazy-cat: ")
  (time (fibonacci-n-digit-number-lazy-cat 1000))
  (println "Solution:" (fibonacci-n-digit-number-lazy-cat 1000)))
