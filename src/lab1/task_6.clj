(ns lab1.task-6)

(defn sqr [num]
  (* num num))

(defn get-sum-squared [num]
  (sqr (reduce + 0 (range num))))

(defn get-squares-sum [num]
  (reduce + 0 (map
               (fn [seq-num] (sqr seq-num))
               (range num))))

(defn get-6-reduce [num]
  (-
   (get-sum-squared (+ 1 num))
   (get-squares-sum (+ 1 num))))
;;

(defn get-natural-sequence
  []
  (iterate inc 0))

(defn get-squares-sequence
  [seq]
  (map sqr seq))

(defn get-sum-squares-sequence
  [num]
  (reduce +' (take num (get-squares-sequence (get-natural-sequence)))))

(defn get-sum-sequence-square
  [num]
  (sqr (reduce +' (take num (get-natural-sequence)))))

(defn get-6-seq
  [num]
  (-' (get-sum-sequence-square (+ num 1)) (get-sum-squares-sequence (+ num 1))))
;;

(defn sum-square-difference-monolite
  "Calculates  the difference between the sum of the squares 
  and the square of the sum
  "
  [n]
  (let [sqr-l (fn [num]
                (*' num num))]
    (loop [sum 0 sum-sq 0 next 0]
      (if
       (< next (+ n 1))
        (recur (+' sum next) (+' sum-sq (sqr-l next)) (inc next))
        (- (sqr-l sum) sum-sq)))))
;;

(defn sum-square-difference-monolite-recur
  "Calculates  the difference between the sum of the squares 
  and the square of the sum
  "
  ([num]
   (sum-square-difference-monolite-recur 0 0 num))
  ([sum sum-sq privios]
   (let [sqr-l (fn [num]
                 (*' num num))]
     (if
      (> privios 0)
       (sum-square-difference-monolite-recur (+' sum privios) (+' sum-sq (sqr-l privios)) (dec privios))
       (- (sqr-l sum) sum-sq)))))
;;

(defn report-6
  []
  (println "num = 10000")
  (print "sum-square-difference tail-recursive ")
  (time (sum-square-difference-monolite 10000))
  (println "Result:" (sum-square-difference-monolite 10000))
  (println "num = 1000")
  (print "sum-square-difference-recursive ")
  (time (sum-square-difference-monolite-recur 1000))
  (println "Result:" (sum-square-difference-monolite-recur 1000))
  (print "sum-square-difference-seq ")
  (time (get-6-seq 10000))
  (println "Result:" (get-6-seq 10000))
  (print "sum-square-difference-resuced ")
  (time (get-6-reduce 1000))
  (println "Result:" (get-6-reduce 10000)))
