(ns lab1.core-test
  (:require [clojure.test :refer :all]
            [lab1.task-6 :refer :all]
            [lab1.task-25 :refer :all]))

(deftest task-6-test
  (testing "sum-square-difference-monolite"
    (is (= (sum-square-difference-monolite 1) 0))
    (is (= (sum-square-difference-monolite 3) 22))
    (is (= (sum-square-difference-monolite 100) 25164150))
    (is (= (sum-square-difference-monolite-recur 100) 25164150))
    (is (= (get-6-seq 100) 25164150))
    (is (= (get-6-reduce 100) 25164150))))

(deftest task-25-test
  (testing "n-digit-fibonacci-number-monolite"
    (is (= (n-digit-fibonacci-number-monolite 1) 1))
    (is (= (n-digit-fibonacci-number-monolite 2) 7))
    (is (= (n-digit-fibonacci-number-monolite 100) 476))
    (is (= (n-digit-fibonacci-number-monolite-recur 100) 476))
    (is (= (fibonacci-n-digit-number 100) 476))
    (is (= (fibonacci-n-digit-number-lazy-cat 100) 476))))